FROM debian:stretch-slim

# File Author / Maintainer
MAINTAINER paperbenni

RUN \
# MAN folder needed for jre install
     mkdir -p /usr/share/man/man1 \
  && mkdir -p /sheep/cache \
# Install JRE and curl
  && apt-get update \
  && apt-get install -y --no-install-recommends \
	openjdk-8-jre-headless \
	curl \
	#Blender dependencies
	libsdl1.2debian \
	libxxf86vm1 \
	libgl1-mesa-glx \
	libglu1-mesa \
	libxi6 \
 	libxrender1 \
	libxfixes3

ADD start.sh /sheep/start.sh
RUN chmod +x /sheep/start.sh

WORKDIR /sheep

ENV user_name "paperbenni"
ENV user_password "paperrender"
ENV cpu "2"

CMD ./start.sh